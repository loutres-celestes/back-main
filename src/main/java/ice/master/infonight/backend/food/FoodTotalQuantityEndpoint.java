package ice.master.infonight.backend.food;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.neo4j.driver.v1.AccessMode;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import ice.master.infonight.backend.bdd.Neo4jDatabase;

/**
 * REST service displaying total quantity of food.
 */
@Path("/miam")
public class FoodTotalQuantityEndpoint {
	@GET
    @Path("/total")
    @Produces(MediaType.TEXT_PLAIN)
    public String greet() {
    	try(Session session =Neo4jDatabase.driver.session(AccessMode.READ)){
    		StatementResult result = session.run("MATCH (f:Food) RETURN count(f) AS total");
        	String end = "";
        	while (result.hasNext()) {
        		Record step = result.next();
    			int total = step.get("total").asInt();
        		end = "Total number of food portions = " + total;
    		}
        	return end;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return e.getMessage();
    	}
    }
}
