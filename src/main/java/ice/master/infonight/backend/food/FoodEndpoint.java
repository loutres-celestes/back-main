package ice.master.infonight.backend.food;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.neo4j.driver.v1.AccessMode;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import com.google.gson.Gson;

import ice.master.infonight.backend.bdd.Neo4jDatabase;
import ice.master.infonight.backend.core.Food;

@Path("/miam")
public class FoodEndpoint {
	
	private Gson gson = new Gson();
	
	@GET
    @Path("/all")
    @Produces(MediaType.TEXT_PLAIN)
    public String all() {
		
		StringBuffer repB = new StringBuffer();
		repB.append("{\"Foods\": [");
		
		try(Session neo = Neo4jDatabase.driver.session(AccessMode.READ)){
			StatementResult result = neo.run("MATCH (f:Food) RETURN ID(f) as id, f.name as name, f.quantity as qty, f.initialQty as initQty");
			if(result.hasNext())
				repB.append(gson.toJson(makeFood(result.next())));
			while(result.hasNext()) {
				repB.append(",");
				repB.append(gson.toJson(makeFood(result.next())));
			}
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
		repB.append("]}");
    	return repB.toString();
    }
	
	private Food makeFood(Record rec) {
		return new Food(rec.get("id").asInt(), rec.get("name").asString(), rec.get("qty").asInt(), rec.get("initQty").asInt());
	}
	
}
