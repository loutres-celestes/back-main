package ice.master.infonight.backend.food;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.neo4j.driver.v1.AccessMode;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import ice.master.infonight.backend.bdd.Neo4jDatabase;

/**
 * REST service displaying the food per food type.
 */
@Path("/miam")
public class FoodPerFoodTypeEndpoint {
	@GET
    @Path("/FoodPerFoodType")
    @Produces(MediaType.TEXT_PLAIN)
    public String greet() {
    	try(Session session = Neo4jDatabase.driver.session(AccessMode.READ)){
    		StatementResult result = session.run("MATCH (f:Food)-->(ft:FoodType) RETURN f.name AS fName, ft.name AS ftName");
        	String end = "Food > FoodType";
        	while (result.hasNext()) {
        		Record step = result.next();
        		String fName = step.get("fName").asString();
        		String ftName = step.get("ftName").asString();
        		end += "\n\t" + fName + "\t>   "+ftName;
    		}
        	return end;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return e.getMessage();
    	}
    }
}