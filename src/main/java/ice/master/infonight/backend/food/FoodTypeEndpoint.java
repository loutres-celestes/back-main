package ice.master.infonight.backend.food;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.neo4j.driver.v1.AccessMode;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import ice.master.infonight.backend.bdd.Neo4jDatabase;

/**
 * REST service displaying food type.
 */
@Path("/miam")
public class FoodTypeEndpoint {
	@GET
    @Path("/foodType")
    @Produces(MediaType.TEXT_PLAIN)
    public String greet() {
    	try(Session session =Neo4jDatabase.driver.session(AccessMode.READ)){
    		StatementResult result = session.run("MATCH (ft:FoodType) RETURN ft.name AS name");
        	String end = "Food types :";
        	while (result.hasNext()) {
        		Record step = result.next();
    			String name = step.get("name").asString();
        		end += "\n\t- " + name;
    		}
        	return end;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return e.getMessage();
    	}
    }
}