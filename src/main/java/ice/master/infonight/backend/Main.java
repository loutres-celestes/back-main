package ice.master.infonight.backend;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import ice.master.infonight.backend.cors.CORSFilter;

/**
 * Main class.
 *
 */
public class Main {
	
	// Base URI the Grizzly HTTP server will listen on
	public static final String BASE_URI = "http://0.0.0.0:5150/"; 

	/**
	 * Starts Grizzly HTTP server exposing JAX-RS resources defined in this
	 * application.
	 * 
	 * @return Grizzly HTTP server.
	 */
	public static HttpServer startServer(String url) {
		// create a resource config that scans for JAX-RS resources and providers
		// in ice.master.loe.rest package
		final ResourceConfig rc = new ResourceConfig().packages("ice.master.infonight.backend");
		rc.register(CORSFilter.class);
		
		// create and start a new instance of grizzly http server
		// exposing the Jersey application at BASE_URI
		return GrizzlyHttpServerFactory.createHttpServer(URI.create(url), rc);
	}

	/**
	 * Main method.
	 * 
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		String url = BASE_URI;
			
		if (args.length > 0)
			url = args[0];
		
		final HttpServer server = startServer(url);
		
		Thread.currentThread().join();
		server.shutdownNow();
	}
}