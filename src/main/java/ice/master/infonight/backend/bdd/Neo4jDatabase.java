package ice.master.infonight.backend.bdd;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;

public final class Neo4jDatabase {
	
	private Neo4jDatabase() {
		// singleton: does not make sense to instantiate it
	}

	public static final Driver driver = GraphDatabase.driver("bolt://" + System.getenv("NEO4J_SERV"), AuthTokens.basic(System.getenv("NEO4J_USERNAME"), System.getenv("NEO4J_PASSWORD")));
}
