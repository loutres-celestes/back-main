package ice.master.infonight.backend.errors;

/**
 * Utility functions to create JSON error message.
 */
public final class Errors {
	
	private Errors() {
		// utility class: should not be instantiated
	}
	
	/**
	 * Returns a JSON message for a non-found resource.
	 * 
	 * @param message
	 * 			A precise message explaining what is not found.
	 * 
	 * @return an error message in JSON
	 */
	public static String notFound(String message) {
		return error(404, message);
	}
	
	/**
	 * Returns a JSON message for an internal error.
	 * 
	 * @param e
	 * 			The exception related to the error.
	 * 
	 * @return an error message in JSON
	 */
	public static String internalError(Exception e) {
		return error(500, e.getMessage());
	}
	
	/**
	 * Formats a JSON error message.
	 * 
	 * @param code
	 * 			The code of the error.
	 * @param message
	 * 			The explanation of the error.
	 * 
	 * @return a JSON error message
	 */
	private static String error(int code, String message) {
		return "{\n" +
			   "  code: " + code + ",\n" +
			   "  message: " + message + "\n" +
			   "}";
	}

}
