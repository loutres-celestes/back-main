package ice.master.infonight.backend.missions;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import com.google.gson.Gson;

import ice.master.infonight.backend.bdd.Neo4jDatabase;
import ice.master.infonight.backend.core.Device;
import ice.master.infonight.backend.core.Mission;
import ice.master.infonight.backend.core.Step;
import ice.master.infonight.backend.errors.Errors;

/**
 * REST service providing information about missions.
 */
@Path("/missions/{id}")
public class MissionEndpoint {
	
	/** Provides conversion to JSON */
	private final Gson gson = new Gson();
	
	/** Provides access to missions data */
	private final Driver driver = Neo4jDatabase.driver;
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getMission(@PathParam("id") int id) {
		try (Session session = driver.session()) {
			StatementResult result = session.run(
					"MATCH (m: Mission) " +
					"WHERE ID(m) = " + id + " " +
					"OPTIONAL MATCH (s: Step)<-[i: IS_MADE_OF]-(m) " +
					"OPTIONAL MATCH (d: Device)<-[REQUIRE]-(m) " +
					"RETURN ID(m) AS id, m.name AS name, m.date AS date, m.caption AS caption, "
						 + "CASE s WHEN NULL THEN [] ELSE COLLECT({id: ID(s), title: s.title, desc: s.description, completed: s.isCompleted}) END AS steps, "
						 + "CASE d WHEN NULL THEN [] ELSE COLLECT({id: ID(d), name: d.name, battery: d.battery}) END AS devices");
			
			if (! result.hasNext())
				return Errors.notFound("mission with id " + id + " cannot be found");
				
			Record found = result.next();
			
			Mission mission = new Mission(found.get("id").asInt(), found.get("name").asString(), found.get("caption").asString(), found.get("date").asString(), null);

			List<Step> steps = found.get("steps").asList(Records::toStep, new ArrayList<Step>());
			mission.getSteps().addAll(steps);
			
			List<Device> devices = found.get("devices").asList(Records::toDevice, new ArrayList<Device>());
			mission.getRequiredDevices().addAll(devices);
			
			return gson.toJson(mission);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return Errors.internalError(e);
		}
	}
	
	@GET
	@Path("/steps")
	@Produces(MediaType.TEXT_PLAIN)
	public String getSteps(@PathParam("id") int missionId) {
		try (Session session = driver.session()) {
			StatementResult result = session.run(
					"MATCH (m: Mission)-[:IS_MADE_OF]->(s: Step) " +
					"WHERE ID(m) = " + missionId + " " +
					"RETURN ID(s) AS id, s.title AS title, s.description AS desc, s.isCompleted AS completed");
			
			List<Step> steps = new ArrayList<>();
			
			while (result.hasNext()) {
				Record step = result.next();
				steps.add(new Step(step.get("id").asInt(), step.get("title").asString(), step.get("desc").asString(), step.get("completed").asBoolean()));
			}
				
			return gson.toJson(steps);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return Errors.internalError(e);
		}
	}
	
	@GET
	@Path("/devices")
	@Produces(MediaType.TEXT_PLAIN)
	public String getDevices(@PathParam("id") int missionId) {
		try (Session session = driver.session()) {
			StatementResult result = session.run(
					"MATCH (m: Mission)-[:REQUIRE]->(d: Device) " +
					"WHERE ID(m) = " + missionId + " " +
					"RETURN ID(d) AS id, d.name AS name, d.battery AS battery");
			
			List<Device> devices = new ArrayList<>();
			
			while (result.hasNext()) {
				Record device = result.next();
				devices.add(new Device(device.get("id").asInt(), device.get("name").asString(), device.get("battery").asInt()));
			}
				
			return gson.toJson(devices);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return Errors.internalError(e);
		}
	}

}
