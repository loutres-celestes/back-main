package ice.master.infonight.backend.missions;

import org.neo4j.driver.v1.Value;

import ice.master.infonight.backend.core.Device;
import ice.master.infonight.backend.core.Step;

public final class Records {

	private Records() {
		// utility class, should not be instantiate
	}
	
	public static Step toStep(Value record) {
		return new Step(record.get("id").asInt(), record.get("title").asString(), record.get("description").asString());
	}
	
	public static Device toDevice(Value record) {
		return new Device(record.get("id").asInt(), record.get("name").asString(), record.get("battery").asInt());
	}
	
}
