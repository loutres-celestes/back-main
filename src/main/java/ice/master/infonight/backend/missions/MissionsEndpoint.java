package ice.master.infonight.backend.missions;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import com.google.gson.Gson;

import ice.master.infonight.backend.bdd.Neo4jDatabase;
import ice.master.infonight.backend.core.Device;
import ice.master.infonight.backend.core.Mission;
import ice.master.infonight.backend.core.Step;
import ice.master.infonight.backend.errors.Errors;
@Path("/missions")
public class MissionsEndpoint {
	
	/** Provides conversion to JSON */
	private final Gson gson = new Gson();
	
	/** Provides access to missions data */
	private final Driver driver = Neo4jDatabase.driver;
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String allMissions() {
		try (Session session = driver.session()) {
			StatementResult result = session.run(
					"MATCH (m: Mission) " +
					"OPTIONAL MATCH (s: Step)<-[i: IS_MADE_OF]-(m) " +
					"OPTIONAL MATCH (d: Device)<-[REQUIRE]-(m) " +
					"RETURN ID(m) AS id, m.name AS name, m.date AS date, m.caption AS caption, " +
					"CASE s WHEN NULL THEN [] ELSE COLLECT({id: ID(s), title: s.title, desc: s.description, caption: s.caption, completed: s.isCompleted}) END AS steps, " +
					"CASE d WHEN NULL THEN [] ELSE COLLECT({id: ID(d), name: d.name, battery: d.battery}) END AS devices");
			
			List<Mission> missions = new ArrayList<>();
			
			while (result.hasNext()) {
				Record mission = result.next();
				Mission newMission = new Mission(mission.get("id").asInt(), mission.get("name").asString(), mission.get("caption").asString(), mission.get("date").asString(), null);
				
				List<Step> steps = mission.get("steps").asList(Records::toStep, new ArrayList<Step>());
				newMission.getSteps().addAll(steps);
				
				List<Device> devices = mission.get("devices").asList(Records::toDevice, new ArrayList<Device>());
				newMission.getRequiredDevices().addAll(devices);
				
				missions.add(newMission);
			}
			return gson.toJson(missions);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return Errors.internalError(e);
		}
	}
	
}
