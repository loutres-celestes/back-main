package ice.master.infonight.backend.core;

public class Food {
	
	private int id;
	
	private String name;
	
	private int quantity;
	
	private int initialQuantity;

	public Food(int id, String name, int quantity, int initialQuantity) {
		this.id = id;
		this.name = name;
		this.quantity = quantity;
		this.initialQuantity = initialQuantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getInitialQuantity() {
		return initialQuantity;
	}

	public void setInitialQuantity(int initialQuantity) {
		this.initialQuantity = initialQuantity;
	}

	public int getId() {
		return id;
	}
	
	public double getRemainingPercentage() {
		return getQuantity() / getInitialQuantity();
	}
	
}
