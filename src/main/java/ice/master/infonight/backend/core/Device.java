package ice.master.infonight.backend.core;

public class Device {
	
	private int id;
	
	private String name;
	
	private int batteryPercentage;

	public Device(int id, String name, int batteryPercentage) {
		this.id = id;
		this.name = name;
		this.batteryPercentage = batteryPercentage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBatteryPercentage() {
		return batteryPercentage;
	}

	public void setBatteryPercentage(int batteryPercentage) {
		this.batteryPercentage = batteryPercentage;
	}

	public int getId() {
		return id;
	}
	
}
