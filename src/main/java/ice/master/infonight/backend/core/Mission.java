package ice.master.infonight.backend.core;

import java.util.ArrayList;
import java.util.List;

public class Mission {
	
	private int id;
	
	private String name;
	
	private String caption;
	
	private String date;
	
	private Location location;
	
	private List<Step> steps;
	
	private List<Device> requiredDevices;

	public Mission(int id, String name, String caption, String date, Location location) {
		this.id = id;
		this.name = name;
		this.caption = caption;
		this.date = date;
		this.location = location;
		this.steps = new ArrayList<>();
		this.requiredDevices = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}
	
	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void setLocation(Location location) {
		this.location = location;
	}

	public List<Step> getSteps() {
		return steps;
	}

	public List<Device> getRequiredDevices() {
		return requiredDevices;
	}
	
}
