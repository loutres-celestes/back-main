package ice.master.infonight.backend.core;

public class Step {
	
	private int id;
	
	private String title;
	
	private String description;
	
	private boolean isCompleted;
	
	public Step(int id, String title, String description) {
		this(id, title, description, false);
	}

	public Step(int id, String title, String description, boolean isCompleted) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.isCompleted = isCompleted;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public int getId() {
		return id;
	}
	
}
