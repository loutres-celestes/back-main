package ice.master.infonight.backend.core;

public class Location {
	
	private int id;
	
	private double x;
	
	private double y;

	public Location(int id, double x, double y) {
		this.id = id;
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public int getId() {
		return id;
	}
	
}
