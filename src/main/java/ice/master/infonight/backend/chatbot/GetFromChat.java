package ice.master.infonight.backend.chatbot;

import java.util.HashMap;
import java.util.Map;

import org.neo4j.driver.v1.AccessMode;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import ice.master.infonight.backend.bdd.Neo4jDatabase;

public class GetFromChat {
	
	public static String getTotalOfFruits() {
		try(Session session = Neo4jDatabase.driver.session(AccessMode.READ)){
    		StatementResult result = session.run("MATCH (f:Food) RETURN count(*) AS total");
        	
    		if(!result.hasNext())
        		return "Il n'y a plus de fruits à manger";
        	
    		return "Il reste " + result.next().get("total").asInt() + " fruits";
        	
    	}catch(Exception e) {
    		e.printStackTrace();
    		return e.getMessage();
    	}
	}

	public static String getTotalOfFood(String name) {
		try(Session session = Neo4jDatabase.driver.session(AccessMode.READ)){
    		Map<String, Object> params = new HashMap<>();
    		params.put("foodName", name);
    		StatementResult result = session.run("MATCH (f:Food {name:$foodName}) RETURN f.name AS fName, count(*) AS total", params);
        	
    		if(!result.hasNext())
        		return name + " n'existe pas.";
        	
    		String end = "Il reste ";
        	while (result.hasNext()) {
        		Record step = result.next();
        		int total = step.get("total").asInt();
        		end += total + " " + name;
    		}
        	
        	return end;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return e.getMessage();
    	}
	}

	public static String getTotalOfDevice(String name) {
		try(Session session = Neo4jDatabase.driver.session(AccessMode.READ)){
    		Map<String, Object> params = new HashMap<>();
    		params.put("foodName", name);
    		StatementResult result = session.run("MATCH (d: Device {name: '" + name + "'}) RETURN count(*) AS total");
        	
    		if(!result.hasNext())
        		return name + " n'existe pas.";
    		
    		return "Il y a " + result.next().get("total").asInt() + " " + name;
        	
    	}catch(Exception e) {
    		e.printStackTrace();
    		return e.getMessage();
    	}
	}
	
	public static String getTool(String name) {
		try(Session session = Neo4jDatabase.driver.session(AccessMode.READ)){
    		Map<String, Object> params = new HashMap<>();
    		params.put("toolName", name);
    		StatementResult result = session.run("MATCH (d:Device {name:$toolName}) "
    				+ "RETURN d.name AS dName, d.battery AS battery, d.usable AS usable", params);
        	
    		if(!result.hasNext())
        		return name + " n'existe pas.";
	    	
    		String end = "L'outil "+name;
    		while (result.hasNext()) {
	    		Record step = result.next();
	    		int dBattery = step.get("battery").asInt();
	    		boolean usable = step.get("usable").asBoolean();
	    		if (usable)
	    			end += " est utilisable avec " + dBattery + "% de batterie.";
	    		else
	    			end += " est inutilisable.";
    		}
        	return end;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return e.getMessage();
    	}
	}
}
