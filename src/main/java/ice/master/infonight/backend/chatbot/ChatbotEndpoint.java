package ice.master.infonight.backend.chatbot;

import java.io.IOException;
import java.io.StringReader;
import java.net.URLEncoder;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import ice.master.infonight.backend.errors.Errors;

@Path("/chatbot")
public class ChatbotEndpoint {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getAnswer(@QueryParam("sentence") String sentence) throws IOException {
		try {
			JsonReader reader = new JsonReader(new StringReader(interpret(sentence)));
			reader.setLenient(true);
			JsonObject interpretation = new JsonParser().parse(reader).getAsJsonObject();
			
			String intent = interpretation.get("intent").getAsJsonObject().get("name").getAsString();

			JsonObject firstEntity = interpretation.get("entities").getAsJsonArray().get(0).getAsJsonObject();
			String value = firstEntity.get("value").getAsString();
			String entity = firstEntity.get("entity").getAsString();
			
			return answerFor(intent, entity, value);
		}
		catch (Exception e) {
			e.printStackTrace();
			return Errors.internalError(e);
		}
	}
	
	/**
	 * Ask Rasa NLU for an anwser.
	 * 
	 * @param sentence
	 * 			The sentence sent by the user.
	 * 
	 * @return Rasa NLU's interpretation of the sentence
	 */
	private String interpret(String sentence) throws IOException {
		  HttpHost target = new HttpHost(System.getenv("RASA_IP"), Integer.parseInt(System.getenv("RASA_PORT")), "http");
		  DefaultHttpClient httpclient = new DefaultHttpClient();
		      
	      // specify the get request
	      HttpGet getRequest = new HttpGet("/parse?project=nuit_info&q=" + URLEncoder.encode(sentence));
	      HttpResponse httpResponse = httpclient.execute(target, getRequest);
	      HttpEntity entity = httpResponse.getEntity();
	      return EntityUtils.toString(entity);
	}
	
	private String answerFor(String intent, String entity, String value) {
		System.out.println(intent + "/" + entity + "/" + value);
		if (intent.equals("combien")) {
			if (entity.equals("miam")) {
				if (value.equals("fruits")) {
					return GetFromChat.getTotalOfFruits();
				}
				return GetFromChat.getTotalOfFood(value);
			}
			else if (entity.equals("outil")) {
				return GetFromChat.getTotalOfDevice(value);
			}
		}
		else if (intent.equals("charge")) {
			if (entity.equals("outil")) {
				return GetFromChat.getTool(value);
			}
		}
		else if (intent.equals("etat")) {
			if (entity.equals("outil")) {
				return GetFromChat.getTool(value);
			}
		}
		return "Je ne sais que vous dire.";
	}

}
