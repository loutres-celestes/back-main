package ice.master.infonight.backend.devices;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.neo4j.driver.v1.AccessMode;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import ice.master.infonight.backend.bdd.Neo4jDatabase;

/**
 * REST service displaying the tools.
 * example: [0123456789] RobotMomo : 98% de batterie
 * 			[9876543210] EolienneLili : 33% de batterie
 */
@Path("/greet")
public class BaseOutilsEndpoint {
	
    @GET
    @Path("/tools")
    @Produces(MediaType.TEXT_PLAIN)
    public String greet() {
    	try(Session session =Neo4jDatabase.driver.session(AccessMode.READ)){
    		StatementResult result = session.run("MATCH (d:Device) RETURN ID(d) AS id, d.name AS name, d.battery AS battery");
        	String end = "My tools :\n";
        	while (result.hasNext()) {
        		Record step = result.next();
    			int id = step.get("id").asInt();
    			String name = step.get("name").asString();
    			int battery = step.get("battery").asInt();
        		end += ("\t["+ id +"] "+ name +" : "+ battery +"% de batterie");
    		}
        	return end;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return e.getMessage();
    	}
    }

}
