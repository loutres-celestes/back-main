FROM openjdk:8-jre

WORKDIR /opt/app

ADD target/eevee-assist-backend.jar ./

ENTRYPOINT ["java", "-jar", "eevee-assist-backend.jar"]